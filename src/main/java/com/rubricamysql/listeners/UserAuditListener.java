package com.rubricamysql.listeners;

import com.rubricamysql.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;


@Component
public class UserAuditListener {

    static Logger logger = LoggerFactory.getLogger(UserAuditListener.class);

    @PreRemove
    public void onPreRemove(User user) {
        logger.info(" {} has been removed",user.getName());
    }
      
    @PreUpdate
    private void beforeAnyUpdate(User user) {
        if (user.getName().equals("Drake3")) {
            logger.info(" {} has been updated",user.getName());
        } else {         
            logger.info(" {} is not Drake3",user.getName());
        }
    }

    @PrePersist
    private void beforeCreate(User user) {
        logger.info("User created");
    }
}
