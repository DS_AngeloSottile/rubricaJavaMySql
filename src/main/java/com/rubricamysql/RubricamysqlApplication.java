package com.rubricamysql;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class RubricamysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(RubricamysqlApplication.class, args);
	}

	@Bean
	public OpenAPI getOpenAPI() {
		return new OpenAPI()
				.components(new Components())
				.info(new Info().title("Rubrica API").version("1.0.0")
						.license(new License().name("Apache 2.0").url("http://springdoc.org")));
	}
}
