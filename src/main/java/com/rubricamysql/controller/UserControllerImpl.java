package com.rubricamysql.controller;


import com.rubricamysql.model.User;
import com.rubricamysql.model.dto.UserDto;
import com.rubricamysql.service.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserControllerImpl implements UserController {

    @Autowired
    UserServiceInterface userService;

    @Override
    @PostMapping(value = "/storeUser")
    public ResponseEntity<User> storeAnime(UserDto userRequest) {

        User user = userService.store(userRequest);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @Override
    @GetMapping(value = "/getUsersDAO")
    public ResponseEntity<List<User>> getUsersDao() {

        List<User> users = userService.getUsers();

        return new ResponseEntity<>(users, HttpStatus.OK);
    }
    @Override
    @GetMapping(value = "/getUsersDSL")
    public ResponseEntity<List<User>> getUsersDSL() {

        List<User> users = userService.getUsersDSL();

        return new ResponseEntity<>(users, HttpStatus.OK);
    }
    @Override
    @GetMapping(value = "/getUserByName/{name}")
    public ResponseEntity<List<User>> getUserByName(String name) {

        List<User> users = userService.getUserByName(name);

        return new ResponseEntity<>(users, HttpStatus.OK);
    }

}
