package com.rubricamysql.controller;

import com.rubricamysql.model.User;
import com.rubricamysql.model.dto.UserDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UserController {

    @Operation(summary = "Create a new User with all data")
    @ApiResponses(value = {
            @ApiResponse(
            responseCode = "200",
            description = "the method create a new Anime",
            content = {
                    @Content(
                            mediaType = "application/json",
                            schema = @Schema(implementation = User.class))
            }),
    })
    ResponseEntity<User> storeAnime(@RequestBody UserDto userRequest);

    @Operation(summary = "Retrieve all users from DB")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method retrieve all users",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = User.class))
                    }),
    })
    ResponseEntity<List<User>> getUsersDao();

    @Operation(summary = "Retrieve all users from DB")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method retrieve all users",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = User.class))
                    }),
    })
    ResponseEntity<List<User>> getUsersDSL();

    @Operation(summary = "Retrieve an user by name")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "the method retrieved an user by name",
                    content = {
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = User.class))
                    }),
    })
    ResponseEntity<List<User>> getUserByName(@PathVariable String name);
}
