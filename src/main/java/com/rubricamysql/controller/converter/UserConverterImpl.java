package com.rubricamysql.controller.converter;

import com.rubricamysql.model.User;
import com.rubricamysql.model.dto.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserConverterImpl {

    public User convertDtoToTo(UserDto userDto){

        return User.builder()
                .name(userDto.getName())
                .surname(userDto.getSurname())
                .email(userDto.getEmail())
                .build();
    }
}
