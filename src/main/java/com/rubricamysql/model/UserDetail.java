package com.rubricamysql.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="userdetails")
@Getter
@Setter
public class UserDetail {


	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private Integer id;

	@Column(name="age",nullable=true)
	private int age;

	@Column(name="city",nullable = true)
	private String city;
	
	@Column(name ="birth_date", nullable = true)
	private LocalDateTime birthDate;

	@OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id",referencedColumnName = "id", nullable = true)
	@JsonBackReference
	private User user;
	
	public UserDetail() {

	}
}
