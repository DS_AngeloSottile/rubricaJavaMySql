package com.rubricamysql.model.dto;

import com.rubricamysql.model.HelpCenter;
import com.rubricamysql.model.Telephone;
import com.rubricamysql.model.UserDetail;
import lombok.Data;

import java.util.List;

@Data
public class UserDto {

	private String name;

	private String surname;

	private String email;

	private UserDetail detail; //one to one

	private List<Telephone> numbers; //one to many

	private List<HelpCenter> helpCenters; //many to many

}
