package com.rubricamysql.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="helpcenters")
@Data
public class HelpCenter {
	
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private Integer id;
	
	@Column(name="name")
	private String name;
	
	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable
    (
		name="user_helpcenter", 
		joinColumns = {@JoinColumn(name="helpcenter_id", referencedColumnName="id")},  
		inverseJoinColumns = {@JoinColumn(name="user_id", referencedColumnName="id")}
    )
	@JsonBackReference
	private List<User> userList;
}
