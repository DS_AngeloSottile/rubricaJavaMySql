package com.rubricamysql.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.rubricamysql.listeners.UserAuditListener;
import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="users")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(UserAuditListener.class)
public class User {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@NotBlank(message = "Name cannot be blank")
	@Column(name="name",nullable=false)
	private String name;

	@Column(name="surname",nullable=false)
	private String surname;

	@Column(name="email",nullable=false)
	private String email;

	@Column(name="updatedAt")
	@UpdateTimestamp
	private Date updatedAt;

	@Column(name="deletedAt")
	private Date deletedAt;

	@OneToOne( fetch = FetchType.LAZY,  mappedBy = "user",orphanRemoval = true, cascade = CascadeType.ALL) //questo user è il nome che hai in user è molto semplice usando le convenzioni di Laravel
	@JsonManagedReference
	private UserDetail detail; //one to one
	
	@OneToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy="user", orphanRemoval = true)
	@JsonManagedReference
	private List<Telephone> numbers; //one to many
	
	@ManyToMany(cascade = CascadeType.DETACH)
	@JoinTable
	(
		name="user_helpcenter", 
		joinColumns = {@JoinColumn(name="user_id", referencedColumnName="id")},  
		inverseJoinColumns = {@JoinColumn(name="helpcenter_id", referencedColumnName="id")}
	)
	private List<HelpCenter> helpCenters; //many to many
	
}
