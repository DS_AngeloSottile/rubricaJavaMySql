package com.rubricamysql.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="telephones")
@Data
public class Telephone {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;

	@Column(name="number",nullable=false)
	private String number;
 
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName = "id",nullable = true)
	@JsonBackReference
	private User user;
}
