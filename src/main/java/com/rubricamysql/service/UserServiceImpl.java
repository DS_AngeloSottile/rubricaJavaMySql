package com.rubricamysql.service;

import com.querydsl.jpa.impl.JPAQuery;
import com.rubricamysql.controller.converter.UserConverterImpl;
import com.rubricamysql.model.*;
import com.rubricamysql.model.QUser;
import com.rubricamysql.model.dto.UserDto;
import com.rubricamysql.repository.HelpCenterRepository;
import com.rubricamysql.repository.TelephoneRepository;
import com.rubricamysql.repository.UserDetailRepository;
import com.rubricamysql.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("mainUserService")
@Transactional
@CacheConfig(cacheNames = {"users"})
public class UserServiceImpl implements UserServiceInterface{
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserDetailRepository userDetailRepository;
	@Autowired
	private HelpCenterRepository helpCenterRepository;
	@Autowired
	TelephoneRepository telephoneRepository;
	@Autowired
	private UserConverterImpl userConverter;
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User store(UserDto userRequest)
	{		
		User user = userConverter.convertDtoToTo(userRequest);


		if(userRequest.getDetail() != null){
			UserDetail userDetail = userRequest.getDetail();
			userDetail.setUser(user);

			user.setDetail(userDetail);
		}

		if(userRequest.getHelpCenters() != null){
			List<HelpCenter> helpCenterList = userRequest.getHelpCenters();
			user.setHelpCenters(new ArrayList<>());

			for (HelpCenter helpCenterRequest : helpCenterList) {
				HelpCenter helpCenter = helpCenterRepository.findHelpCenterByName(helpCenterRequest.getName());
				user.getHelpCenters().add(helpCenter);
			}
		}

		if(userRequest.getNumbers() != null){

			List<Telephone> telephones = userRequest.getNumbers().stream().peek(telephone -> telephone.setUser(user)).collect(Collectors.toList());
			user.setNumbers(telephones);
		}
		return userRepository.save(user);
	}
	
	public List<User> getUsers(){
		return userRepository.findAll();
	}

	public List<User> getUsersDSL(){
		QUser userTable = QUser.user;
		JPAQuery<User> query = new JPAQuery<>(entityManager);

		return query.from(userTable).where(userTable.name.eq("drake3")).fetch();
	}

	@Override
	public List<User> getUserByName(String name) {
		return userRepository.findByNameNativeQuery(name);
	}
}
