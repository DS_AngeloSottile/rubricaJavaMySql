package com.rubricamysql.service;

import com.rubricamysql.model.User;
import com.rubricamysql.model.dto.UserDto;

import java.util.List;

public interface UserServiceInterface {

    User store(UserDto userRequest);
    List<User> getUsers();
    List<User> getUsersDSL();
    List<User> getUserByName(String name);
}
