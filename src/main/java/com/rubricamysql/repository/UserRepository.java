package com.rubricamysql.repository;

import com.rubricamysql.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

	Iterable<User> findUserByName(String name);

	@Query( value = "SELECT * FROM users u WHERE u.name = :nameParam", nativeQuery = true)
	public List<User> findByNameNativeQuery(@Param("nameParam") String nameParam);
}
