package com.rubricamysql.repository;

import com.rubricamysql.model.HelpCenter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HelpCenterRepository extends CrudRepository<HelpCenter, Integer> {

    HelpCenter findHelpCenterByName(String name);
	
}
